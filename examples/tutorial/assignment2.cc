/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should ∏ve received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "ns3/applications-module.h"
#include "ns3/core-module.h"
#include "ns3/internet-module.h"
#include "ns3/network-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/athstats-helper.h"
#include "ns3/boolean.h"
#include "ns3/command-line.h"
#include "ns3/config.h"
#include "ns3/mobility-helper.h"
#include "ns3/mobility-model.h"
#include "ns3/on-off-helper.h"
#include "ns3/packet-socket-address.h"
#include "ns3/packet-socket-helper.h"
#include "ns3/ssid.h"
#include "ns3/string.h"
#include "ns3/yans-wifi-channel.h"
#include "ns3/yans-wifi-helper.h"
#include "ns3/mobility-module.h"
#include "ns3/internet-stack-helper.h"
#include "ns3/ipv4-address-helper.h"
#include "ns3/multi-model-spectrum-channel.h"
#include "ns3/non-communicating-net-device.h"
#include "ns3/packet-sink-helper.h"
#include "ns3/packet-sink.h"
#include "ns3/propagation-loss-model.h"
#include "ns3/spectrum-wifi-helper.h"
#include "ns3/spectrum-module.h"
#include "ns3/udp-client-server-helper.h"
#include "ns3/waveform-generator-helper.h"
#include "ns3/waveform-generator.h"
#include "ns3/wifi-net-device.h"
#include <fstream>
#include <chrono>
// Network Topology
//
// Ap1 Ap2     
// *  *  
// |  |    
// n1 n3
// 
// STA1 STA2    
// *     *   
// |     |    
// n2    n4 

using namespace ns3;

NS_LOG_COMPONENT_DEFINE("Assignment2");

Ptr<PacketSink> sink1;     //!< Pointer to the packet sink application
uint64_t lastTotalRx1 = 0; //!< The value of the last total received bytes

Ptr<PacketSink> sink2;     //!< Pointer to the packet sink application
uint64_t lastTotalRx2 = 0; //!< The value of the last total received bytes
std::ofstream file1;
std::ofstream file2;
/**
 * Calculate the throughput for first
 */
void
CalculateThroughput1()
{
    Time now = Simulator::Now(); /* Return the simulator's virtual time. */
    double cur = (sink1->GetTotalRx() - lastTotalRx1) * 8.0 /
                 1e5; /* Convert Application RX Packets to MBits. */
    std::cout << "AP1 - " << now.GetSeconds() << "s: \t" << cur << " Mbit/s" << std::endl;
    lastTotalRx1 = sink1->GetTotalRx();

    file1 << ( now.GetSeconds() - 1.1) << " " << cur << std::endl;

    Simulator::Schedule(MilliSeconds(100), &CalculateThroughput1);
}

/**
 * Calculate the throughput
 */
void
CalculateThroughput2()
{
    Time now = Simulator::Now(); /* Return the simulator's virtual time. */
    double cur = (sink2->GetTotalRx() - lastTotalRx2) * 8.0 /
                 1e5; /* Convert Application RX Packets to MBits. */
    std::cout << "AP2 - " << now.GetSeconds() << "s: \t" << cur << " Mbit/s" << std::endl;
    lastTotalRx2 = sink2->GetTotalRx();

    file2 << (now.GetSeconds() - 1.1) << " " << cur << std::endl;

    Simulator::Schedule(MilliSeconds(100), &CalculateThroughput2);
}


int
main(int argc, char* argv[])
{
    std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();

    file1.open("assignment2_AP1.txt");
    file2.open("assignment2_AP2.txt");
    std::string channelNumber1Yans = "1";
    std::string channelNumber2Yans = "2";
    std::string channelNumber1SingleModel = "36";
    std::string channelNumber2SingleModel = "38";
    std::string channelNumber1MultiModel = "40";
    std::string channelNumber2MultiModel = "44";
    uint32_t wirelessModel = 0;            /* 0 YansWiFiChannel, 1 SpectrumWiFiChannel, 2 MultiModelSpectrumChannel*/
    uint32_t nWifi = 1;                    /* Number of stations of AP1*/
    uint32_t nWifi2 = 1;                   /* Number of stations of AP2*/
    uint32_t payloadSize = 1448;           /* Transport layer payload size in bytes. */
    std::string tcpVariant = "TcpNewReno"; /* TCP variant type. */
    std::string phyRate = "HtMcs7";        /* Physical layer bitrate. */
    double simulationTime = 10;            /* Simulation time in seconds. */
    bool pcapTracing = false;              /* PCAP Tracing is enabled or not. */
    bool singleTest = false;               /* 0 Simulate AP 1 STA 1, AP 2, STA 2, 1 Simulate AP 1, STA 1*/
    std::string dataRate = "500Mbps";      /* Application layer datarate. */
    std::string errorModelType = "ns3::YansErrorRateModel";

    CommandLine cmd(__FILE__);
    cmd.AddValue("nWifi", "Number of wifi STA 1 devices", nWifi);
    cmd.AddValue("wirelessModel", "Choose 0 YansWiFiChannel, 1 SpectrumWiFiChannel, 2 MultiModelSpectrumChannel", wirelessModel);
    cmd.AddValue("dataRate", "Application data ate", dataRate);
    cmd.AddValue("nWifi2", "Number of wifi STA 2 devices", nWifi2);
    cmd.AddValue("tcpVariant",
                 "Transport protocol to use: TcpNewReno, "
                 "TcpHybla, TcpHighSpeed, TcpHtcp, TcpVegas, TcpScalable, TcpVeno, "
                 "TcpBic, TcpYeah, TcpIllinois, TcpWestwood, TcpWestwoodPlus, TcpLedbat ",
                 tcpVariant);
    cmd.AddValue("payloadSize", "Payload size in bytes", payloadSize);
    cmd.AddValue("simulationTime", "Simulation time in seconds", simulationTime);
    cmd.AddValue("pcap", "Enable/disable PCAP Tracing", pcapTracing);
    cmd.AddValue("singleTest", "0 Simulate AP 1 STA 1, AP 2, STA 2, 1 Simulate AP 1, STA 1", singleTest);
    cmd.AddValue("channelNumber1Yans", "Channel number for AP1/STA1 yans", channelNumber1Yans);
    cmd.AddValue("channelNumber2Yans", "Channel number for AP2/STA2 yans", channelNumber2Yans);
    cmd.AddValue("channelNumber1SingleModel", "Channel number for AP1/STA1 single model spectrum", channelNumber1SingleModel);
    cmd.AddValue("channelNumber2SingleModel", "Channel number for AP2/STA2 single model spectrum", channelNumber2SingleModel);
    cmd.AddValue("channelNumber1MultiModel", "Channel number for AP1/STA1 multi model spectrum", channelNumber1MultiModel);
    cmd.AddValue("channelNumber2MultiModel", "Channel number for AP2/STA2 multi model spectrum", channelNumber2MultiModel);

    cmd.Parse(argc, argv);

    NS_LOG_UNCOND("Using " << nWifi << " STA for AP 1");
    NS_LOG_UNCOND("Using " << nWifi2 << " STA for AP 2");
    NS_LOG_UNCOND("Using " << wirelessModel << " as wireless model");
    NS_LOG_UNCOND("Using " << dataRate << " as data rate");
    NS_LOG_UNCOND("Using " << tcpVariant << " as tcp");
    NS_LOG_UNCOND("Using " << payloadSize << " as payloadSize");
    NS_LOG_UNCOND("Using " << simulationTime << " (s) as simulationTime");
    NS_LOG_UNCOND("Enable " << pcapTracing << " pcap tracing");
    

    tcpVariant = std::string("ns3::") + tcpVariant;
    // Select TCP variant
    TypeId tcpTid;
    NS_ABORT_MSG_UNLESS(TypeId::LookupByNameFailSafe(tcpVariant, &tcpTid),
                        "TypeId " << tcpVariant << " not found");
    Config::SetDefault("ns3::TcpL4Protocol::SocketType",
                       TypeIdValue(TypeId::LookupByName(tcpVariant)));


    /* Configure TCP Options */
    Config::SetDefault("ns3::TcpSocket::SegmentSize", UintegerValue(payloadSize));

    // Setup P2P Between 2 APs
    // Create 2 APs that are connected with a P2P network
    NodeContainer p2pNodesAP;
    p2pNodesAP.Create(2);

    PointToPointHelper pointToPoint;
    pointToPoint.SetDeviceAttribute("DataRate", StringValue("52Mbps"));
    pointToPoint.SetChannelAttribute("Delay", StringValue("2ms"));

    NetDeviceContainer p2pDevices;
    p2pDevices = pointToPoint.Install(p2pNodesAP);

    InternetStackHelper stack;
    stack.Install(p2pNodesAP);

    Ipv4AddressHelper address;
    address.SetBase("10.1.1.0", "255.255.255.0");
    Ipv4InterfaceContainer interfaces;
    interfaces = address.Assign(p2pDevices);
    //Finish setup, routing table will be populated using Ipv4GlobalRoutingHelper::PopulateRoutingTables();

    NodeContainer wifiStaNodes1;
    wifiStaNodes1.Create(nWifi);

    NodeContainer wifiStaNodes2;
    wifiStaNodes2.Create(nWifi2);

    NodeContainer wifiApNode1 = p2pNodesAP.Get(0);
    NodeContainer wifiApNode2 = p2pNodesAP.Get(1);

    WifiHelper wifi;
    wifi.SetStandard(WIFI_STANDARD_80211n);

    
    //Ssid for the first AP 1
    Ssid ssid1 = Ssid("ns-3-ssid");
    //Ssid for the second AP 2
    Ssid ssid2 = Ssid("ns-3-ssid2");

    WifiMacHelper mac;

    NetDeviceContainer apDevice1;
    NetDeviceContainer staDevices1;

    NetDeviceContainer apDevice2;
    NetDeviceContainer staDevices2;

    //Yans wifi channel
    if(wirelessModel == 0){

        NS_LOG_UNCOND("Model selected: Yans Wifi");

        YansWifiChannelHelper channel = YansWifiChannelHelper::Default();
     
        YansWifiPhyHelper phy;
        phy.SetChannel(channel.Create());
        phy.Set("ChannelSettings", StringValue("{"+channelNumber1Yans+", 0, BAND_2_4GHZ, 0}"));
        NS_LOG_UNCOND("Using channel number for AP/STA1: " << channelNumber1Yans);

        //StaDevices that communicate with AP 1
        mac.SetType("ns3::StaWifiMac", "Ssid", SsidValue(ssid1), "ActiveProbing", BooleanValue(false));
        staDevices1 = wifi.Install(phy, mac, wifiStaNodes1);

        //Access point 1
        mac.SetType("ns3::ApWifiMac", "Ssid", SsidValue(ssid1));
        apDevice1 = wifi.Install(phy, mac, wifiApNode1);

        if(!singleTest){

            phy.Set("ChannelSettings", StringValue("{"+channelNumber2Yans+", 0, BAND_2_4GHZ, 0}"));
            NS_LOG_UNCOND("Using channel number for AP/STA2: " << channelNumber2Yans);

            //StaDevices that communicate with AP 2
            mac.SetType("ns3::StaWifiMac", "Ssid", SsidValue(ssid2), "ActiveProbing", BooleanValue(false));
            staDevices2 = wifi.Install(phy, mac, wifiStaNodes2);

            //Access point 2
            mac.SetType("ns3::ApWifiMac", "Ssid", SsidValue(ssid2));
            apDevice2 = wifi.Install(phy, mac, wifiApNode2);
        }

        /* Enable Traces */
        if (pcapTracing)
        {
            phy.SetPcapDataLinkType(WifiPhyHelper::DLT_IEEE802_11_RADIO);
            phy.EnablePcap("AccessPoint_1_Yans", apDevice1);
            //phy.EnablePcap("Station_1", staDevices1);
            phy.EnablePcap("AccessPoint_2_Yans", apDevice2);
            //phy.EnablePcap("Station_2", staDevices2);
        }

    }
    //Spectrum Channel
    else if(wirelessModel == 1){

        NS_LOG_UNCOND("Model selected: Spectrum Wifi Single Model");

        SpectrumWifiPhyHelper spectrumPhy;

        Ptr<SingleModelSpectrumChannel> spectrumChannel = CreateObject<SingleModelSpectrumChannel>();
        Ptr<FriisPropagationLossModel> lossModel = CreateObject<FriisPropagationLossModel>();
        lossModel->SetFrequency(5.180e9);
        spectrumChannel->AddPropagationLossModel(lossModel);

        Ptr<ConstantSpeedPropagationDelayModel> delayModel =
            CreateObject<ConstantSpeedPropagationDelayModel>();
        spectrumChannel->SetPropagationDelayModel(delayModel);

        spectrumPhy.SetChannel(spectrumChannel);
        spectrumPhy.Set("TxPowerStart", DoubleValue(1)); // dBm  (1.26 mW)
        spectrumPhy.Set("TxPowerEnd", DoubleValue(1));
        spectrumPhy.SetErrorRateModel(errorModelType);

        spectrumPhy.Set("ChannelSettings",
        StringValue(std::string("{"+channelNumber1SingleModel+", 0, BAND_5GHZ, 0}")));
        NS_LOG_UNCOND("Using channel number for AP/STA 1 " << channelNumber1SingleModel);

        mac.SetType("ns3::StaWifiMac", "Ssid", SsidValue(ssid1));
        staDevices1 = wifi.Install(spectrumPhy, mac, wifiStaNodes1);
        mac.SetType("ns3::ApWifiMac", "Ssid", SsidValue(ssid1));
        apDevice1 = wifi.Install(spectrumPhy, mac, wifiApNode1);

        if(!singleTest){
            spectrumPhy.Set("ChannelSettings",
                            StringValue(std::string("{"+channelNumber2SingleModel+", 0, BAND_5GHZ, 0}")));
            NS_LOG_UNCOND("Using channel number for AP/STA 2 " << channelNumber2SingleModel);

            mac.SetType("ns3::StaWifiMac", "Ssid", SsidValue(ssid2));
            staDevices2 = wifi.Install(spectrumPhy, mac, wifiStaNodes2);
            mac.SetType("ns3::ApWifiMac", "Ssid", SsidValue(ssid2));
            apDevice2 = wifi.Install(spectrumPhy, mac, wifiApNode2);
        }


        if (pcapTracing)
        {
            spectrumPhy.SetPcapDataLinkType(WifiPhyHelper::DLT_IEEE802_11_RADIO);
            spectrumPhy.EnablePcap("AccessPoint_1_SingleModel", apDevice1);
            //spectrumPhy.EnablePcap("Station_1_", staDevices1);
            spectrumPhy.EnablePcap("AccessPoint_2_SingleModel", apDevice2);
            //spectrumPhy.EnablePcap("Station_2", staDevices2);
        }

    }
    //Multi Model Spectrum Channel
    else if(wirelessModel == 2){

        NS_LOG_UNCOND("Model selected: Spectrum Wifi Multi Model");

        Ptr<MultiModelSpectrumChannel> multiModelSpectrumChannel;
        SpectrumWifiPhyHelper spectrumPhy;

        multiModelSpectrumChannel = CreateObject<MultiModelSpectrumChannel>();
        Ptr<FriisPropagationLossModel> lossModel = CreateObject<FriisPropagationLossModel>();
        lossModel->SetFrequency(5.180e9);
        multiModelSpectrumChannel->AddPropagationLossModel(lossModel);

        Ptr<ConstantSpeedPropagationDelayModel> delayModel =
            CreateObject<ConstantSpeedPropagationDelayModel>();
        multiModelSpectrumChannel->SetPropagationDelayModel(delayModel);

        spectrumPhy.SetChannel(multiModelSpectrumChannel);
        spectrumPhy.SetErrorRateModel(errorModelType);

        spectrumPhy.Set("ChannelSettings",
                        StringValue(std::string("{"+channelNumber1MultiModel+", 0, BAND_5GHZ, 0}")));
        NS_LOG_UNCOND("Using channel number for AP/STA 1 " << channelNumber1MultiModel);

        mac.SetType("ns3::StaWifiMac", "Ssid", SsidValue(ssid1));
        staDevices1 = wifi.Install(spectrumPhy, mac, wifiStaNodes1);
        mac.SetType("ns3::ApWifiMac", "Ssid", SsidValue(ssid1));
        apDevice1 = wifi.Install(spectrumPhy, mac, wifiApNode1);

        if(!singleTest){
            spectrumPhy.Set("ChannelSettings",
                            StringValue(std::string("{"+channelNumber2MultiModel+", 0, BAND_5GHZ, 0}")));
            NS_LOG_UNCOND("Using channel number for AP/STA 2 " << channelNumber2MultiModel);

            mac.SetType("ns3::StaWifiMac", "Ssid", SsidValue(ssid2));
            staDevices2 = wifi.Install(spectrumPhy, mac, wifiStaNodes2);
            mac.SetType("ns3::ApWifiMac", "Ssid", SsidValue(ssid2));
            apDevice2 = wifi.Install(spectrumPhy, mac, wifiApNode2);
        }

        if (pcapTracing)
        {
            spectrumPhy.SetPcapDataLinkType(WifiPhyHelper::DLT_IEEE802_11_RADIO);
            spectrumPhy.EnablePcap("AccessPoint_1_MultiModel", apDevice1);
            //spectrumPhy.EnablePcap("Station_1", staDevices1);
            spectrumPhy.EnablePcap("AccessPoint_2_MultiModel", apDevice2);
            //phy.EnablePcap("Station_2", staDevices2);
        }

    }


    MobilityHelper mobility;
    Ptr<ListPositionAllocator> positionAlloc = CreateObject<ListPositionAllocator>();
    positionAlloc->Add(Vector(0.0, 0.0, 0.0));
    positionAlloc->Add(Vector( 1.0, 1.0, 0.0));
    
    if(!singleTest){
        positionAlloc->Add(Vector(0.1, 0.0, 0.0));
        positionAlloc->Add(Vector(2.0, 1.0, 0.0));
    }

    mobility.SetPositionAllocator(positionAlloc);
    mobility.SetMobilityModel("ns3::ConstantPositionMobilityModel");
    mobility.Install(wifiApNode1);
    mobility.Install(wifiStaNodes1);
    
    if(!singleTest){
        mobility.Install(wifiApNode2);
        mobility.Install(wifiStaNodes2);
    }

    stack.Install(wifiStaNodes1);

    if(!singleTest){
        stack.Install(wifiStaNodes2);
    }

    Ipv4AddressHelper addressBetweenApStation1;
    addressBetweenApStation1.SetBase("10.0.0.0", "255.255.255.0");
    Ipv4InterfaceContainer apInterface1;
    apInterface1 = addressBetweenApStation1.Assign(apDevice1);
    Ipv4InterfaceContainer staInterface1;
    staInterface1 = addressBetweenApStation1.Assign(staDevices1);

    Ipv4InterfaceContainer apInterface2;
    Ipv4InterfaceContainer staInterface2;
    if(!singleTest){
        
        apInterface2 = addressBetweenApStation1.Assign(apDevice2);
        staInterface2 = addressBetweenApStation1.Assign(staDevices2);
    }

    NS_LOG_UNCOND("AP1 IP Address: " << apInterface1.GetAddress(0));
    NS_LOG_UNCOND("STA1 IP Address: " << staInterface1.GetAddress(0));
    
    if(!singleTest){
        NS_LOG_UNCOND("AP2 IP Address: " << apInterface2.GetAddress(0));
        NS_LOG_UNCOND("STA2 IP Address: " << staInterface2.GetAddress(0));
    }

    /* Populate routing table */
    Ipv4GlobalRoutingHelper::PopulateRoutingTables();
    
    /* Install TCP Receiver on the access point */
    PacketSinkHelper sinkHelper1("ns3::TcpSocketFactory",
                                InetSocketAddress(Ipv4Address::GetAny(), 9));
    ApplicationContainer sinkApp1 = sinkHelper1.Install(wifiApNode1);
    sink1 = StaticCast<PacketSink>(sinkApp1.Get(0));

    ApplicationContainer sinkApp2;
    if(!singleTest){
        PacketSinkHelper sinkHelper2("ns3::TcpSocketFactory",
                                    InetSocketAddress(Ipv4Address::GetAny(), 10));
        sinkApp2 = sinkHelper2.Install(wifiApNode2);
        sink2 = StaticCast<PacketSink>(sinkApp2.Get(0));
    }

    /* Install TCP/UDP Transmitter on the station */
    OnOffHelper server1("ns3::TcpSocketFactory", (InetSocketAddress(apInterface1.GetAddress(0), 9)));
    server1.SetAttribute("PacketSize", UintegerValue(payloadSize));
    server1.SetAttribute("OnTime", StringValue("ns3::ConstantRandomVariable[Constant=1]"));
    server1.SetAttribute("OffTime", StringValue("ns3::ConstantRandomVariable[Constant=0]"));
    server1.SetAttribute("DataRate", DataRateValue(DataRate(dataRate)));
    ApplicationContainer serverApp1 = server1.Install(wifiStaNodes1);
        /* Start Applications 1 */
    sinkApp1.Start(Seconds(0.0));
    serverApp1.Start(Seconds(1.0));
    Simulator::Schedule(Seconds(1.1), &CalculateThroughput1);

    //Config::ConnectWithoutContext("/NodeList/0/DeviceList/*/Phy/MonitorSnifferRx",
    //                            MakeCallback(&MonitorSniffRx));

    /* Install TCP/UDP Transmitter on the station */
    if(!singleTest){
        OnOffHelper server2("ns3::TcpSocketFactory", (InetSocketAddress(apInterface2.GetAddress(0), 10)));
        server2.SetAttribute("PacketSize", UintegerValue(payloadSize));
        server2.SetAttribute("OnTime", StringValue("ns3::ConstantRandomVariable[Constant=1]"));
        server2.SetAttribute("OffTime", StringValue("ns3::ConstantRandomVariable[Constant=0]"));
        server2.SetAttribute("DataRate", DataRateValue(DataRate(dataRate)));
        ApplicationContainer serverApp2 = server2.Install(wifiStaNodes2);
        /* Start Applications 2*/
        sinkApp2.Start(Seconds(0.0));
        serverApp2.Start(Seconds(1.0));
        Simulator::Schedule(Seconds(1.1), &CalculateThroughput2);
    }


    /* Start Simulation */
    Simulator::Stop(Seconds(simulationTime + 1));
    Simulator::Run();
    Simulator::Destroy();

    file1.close();
    file2.close();

    std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
    std::cout << "Time difference = " << std::chrono::duration_cast<std::chrono::seconds>(end - begin).count() << "[s]" << std::endl;

    return 0;
}
