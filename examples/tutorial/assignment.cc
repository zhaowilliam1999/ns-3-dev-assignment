/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "ns3/applications-module.h"
#include "ns3/core-module.h"
#include "ns3/csma-module.h"
#include "ns3/internet-module.h"
#include "ns3/ipv4-global-routing-helper.h"
#include "ns3/network-module.h"
#include "ns3/point-to-point-module.h"

// Default Network Topology
//
//       
// n1   n2   n3   n4
// |    |    |    |
// ================
// LAN 10.1.2.0

using namespace ns3;

NS_LOG_COMPONENT_DEFINE("Assignment");

/**
 * Rx drop callback
 *
 * \param p The dropped packet.
 */
//static void
//RxDrop(Ptr<const Packet> p)
//{
//    NS_LOG_UNCOND("RxDrop at " << Simulator::Now().GetSeconds());
//}

int
main(int argc, char* argv[])
{
    bool verbose = true;
    uint32_t nCsma = 4;
    uint32_t packetSize = 1024;

    CommandLine cmd(__FILE__);
    cmd.AddValue("nCsma", "Number of \"extra\" CSMA nodes/devices", nCsma);
    cmd.AddValue("verbose", "Tell echo applications to log if true", verbose);

    cmd.Parse(argc, argv);

    if (verbose)
    {
        LogComponentEnable("UdpEchoClientApplication", LOG_LEVEL_INFO);
        LogComponentEnable("UdpEchoServerApplication", LOG_LEVEL_INFO);
    }

    NodeContainer csmaNodes;
    csmaNodes.Create(nCsma);

    CsmaHelper csma;
    csma.SetChannelAttribute("DataRate", StringValue("100Mbps"));
    csma.SetChannelAttribute("Delay", TimeValue(NanoSeconds(6560)));

    NetDeviceContainer csmaDevices;
    csmaDevices = csma.Install(csmaNodes);

    InternetStackHelper stack;
    stack.Install(csmaNodes);

    Ipv4AddressHelper address;
    address.SetBase("10.1.2.0", "255.255.255.0");

    Ipv4InterfaceContainer csmaInterfaces;
    csmaInterfaces = address.Assign(csmaDevices);

    UdpEchoServerHelper echoServer(9);
    ApplicationContainer serverApps = echoServer.Install(csmaNodes.Get(0));
    UdpEchoServerHelper echoServer2(10);
    ApplicationContainer serverApps2 = echoServer2.Install(csmaNodes.Get(2));

    serverApps.Start(Seconds(1.0));
    serverApps.Stop(Seconds(10.0));
    serverApps2.Start(Seconds(1.0));
    serverApps2.Stop(Seconds(10.0));

    UdpEchoClientHelper echoClient(csmaInterfaces.GetAddress(0), 9);
    echoClient.SetAttribute("MaxPackets", UintegerValue(55));
    echoClient.SetAttribute("Interval", TimeValue(Seconds(1.1)));
    echoClient.SetAttribute("PacketSize", UintegerValue(packetSize));
    
    ApplicationContainer clientApps = echoClient.Install(csmaNodes.Get(1));

    UdpEchoClientHelper echoClient2(csmaInterfaces.GetAddress(2), 10);
    echoClient2.SetAttribute("MaxPackets", UintegerValue(55));
    echoClient2.SetAttribute("Interval", TimeValue(Seconds(2.0)));
    echoClient2.SetAttribute("PacketSize", UintegerValue(packetSize));

    ApplicationContainer clientApps2 = echoClient2.Install(csmaNodes.Get(3));

    clientApps.Start(Seconds(1.0));
    clientApps.Stop(Seconds(10.0));
    clientApps2.Start(Seconds(2.0));
    clientApps2.Stop(Seconds(10.0));

    Ipv4GlobalRoutingHelper::PopulateRoutingTables();
    
    NS_LOG_UNCOND("Server0 address " << csmaInterfaces.GetAddress(0));
    NS_LOG_UNCOND("Client1 address " << csmaInterfaces.GetAddress(1));
    NS_LOG_UNCOND("Server2 address " << csmaInterfaces.GetAddress(2));
    NS_LOG_UNCOND("Client3 address " << csmaInterfaces.GetAddress(3));

    csma.EnablePcapAll("assignment_1");

    Simulator::Stop();
    Simulator::Run();
    Simulator::Destroy();

    return 0;
}
